# Guest Info System Backend Server Overview

The GuestInfoBackend provides a REST API server that implements an OpenAPI specification and is called by the GuestInfoFrontend. The developers
of GuestInfoFrontend are the clients of GuestInfoBackend.

## Status

The system backend is currently in active development. The database is active and the backend has functioning APIs. The database can take in new data, modify or delete data, or export data. The test suite is fully functional.

## Setting up the Development Environment

**Technologies needed to set up a Development Environment:**

* VSCode - VSCode can be downloaded for any operating system using the link provided below.
    
    [VSCode Setup](https://code.visualstudio.com/download)

* Docker Desktop - Docker Desktop can be installed for any operating system using the link provided below.
    
    [Docker Desktop Setup](https://docs.docker.com/get-started/get-docker/)
* Git - Git can be downloaded for any operating system using the link provided below.
    
    [Git Setup](https://git-scm.com/downloads)

**Instructions to set up a Development Environment:**

1.	Start Docker Desktop and keep it open in the background.
2.	Set up VSCode Extensions.
      * Open VSCode.
      * Use Ctrl-Shift-X to open the Extensions panel in Visual Studio Code.
      * Type `Dev Containers` in the search bar.
      * Install the Dev Containers extension by Microsoft ([Link](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers))
      * Once installed, close VSCode.

**Installing and running the project and its Dev Containers**

1.	Navigate to the GuestInfoBackend project page [GuestInfoBackend](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfobackend/-/tree/main?ref_type=heads) via GitLab and click the ‘Code’ drop down button  `Code -> Open in your IDE -> Visual Studio Code (HTTPS).`
2.	Allow the site to open VSCode.
3.	Select the location where you would like to store the project.
4.	VSCode will ask ‘Would you like to open the cloned repository?’ Select ‘Open’
5.	 A message will pop-up in the bottom right corner. Select "Reopen in container"
      * If you miss the pop-up, click the 2 blue arrows in the bottom-left corner and select "Reopen in Container".
6.	The project now should be opened in a container on your VSCode.
      * If you received an error setting up the container, open your Docker Desktop.
      * Go to `Settings -> Resources -> File Sharing`
      * Click the ‘+’ icon and select the project folder where you saved it.
      * Then click ‘Apply & restart’
      * Open VSCode again and retry opening in a container.
7.	Once the project is opened in a container on VSCode, open a new terminal
8.	To build the project, type in the command line: `./bin/build.sh`
9.	Launch the project using the `./bin/up.sh` command
10.	Click on the `Ports` tab
11.	Make the 10300 port be public (by clicking on the lock)
12.	Click on the globe which should open a new browser tab showing the application.
13.	For additional information on DevContainers please visit the [Development Environment](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfobackend/-/blob/main/docs/developer/development-environment.md?ref_type=heads) link.

## Usage Instructions

The API implemented by this server is in [lib/openapi.yaml](lib/openapi.yaml). The API can be viewed:

* Using the [Swagger Viewer extension for VS Code](https://marketplace.visualstudio.com/items?itemName=Arjun.swagger-viewer) which is installed in the Dev Container for this project.
* Directly in the [GitLab repository for this project](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfobackend).

The [source for the GuestInfoAPI](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfoapi) is where development of the API takes place.

## Folder Information

**.devcontainer**

This folder contains the files needed to run the GuestInfoBackend in a dev container.

**.gitlab/merge_request_templates**

This contains information how to properly commit and merge into the GuestInfoBackend.

**commands**

This folder contains scripts to streamline the running and development of GuestInfoBackend.

**docs/developer**

This folder contains information about various aspects of GuestInfoBackend that will help developers work on the project.

**lib**

This folder has information on how to communicate with the api and the possible causes for errors returned from the api.

**src**

The code for GuestInfoBackend is saved here.

**testing**

The tests for the GuestInfoBackend are stored here and able to run to tested added features.

## Tools

**Developer Guide**

1. Read [LibreFoodPantry.org](https://librefoodpantry.org/)
    and join its Discord server.
2. [lib/openapi.yaml](lib/openapi.yaml) contains the API specification that the server implements. **This file should not be edited**. If changes are needed in the API, development happens in the [source for the GuestInfoAPI](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfoapi).
3. Familiarize yourself with the systems used by this project
  (see Development Infrastructure below).
4. See [the developer cheat-sheet](docs/developer/cheat-sheet.md) for common
  commands you'll likely use.

**Development Infrastructure**

* [Automated Testing](docs/developer/automated-testing.md)
* [Build System](docs/developer/build-system.md)
* [Continuous Integration](docs/developer/continuous-integration.md)
* [Dependency Management](docs/developer/dependency-management.md)
* [Development Environment](docs/developer/development-environment.md)
* [Documentation System](docs/developer/documentation-system.md)
* [Version Control System](docs/developer/version-control-system.md)
* [Visual Studio Code](https://code.visualstudio.com/)
* [Docker Desktop](https://code.visualstudio.com/)

## License

By submitting this issue or commenting on this issue, or contributing any content to this issue, you certify under the Developer Certificate of Origin that the content you post may be licensed under GPLv3 (for code) or CC-BY-SA 4.0 International (for non-code content).

Last updated: November 11th, 2024
