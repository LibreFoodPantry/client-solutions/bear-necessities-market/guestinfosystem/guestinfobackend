//This tests the create new visit function. It should error when visit is pre-existing, invalid, or server issues.
process.env.NODE_ENV = 'test';
let chai = require('chai');
const Visits = require("../Data/visit");
chai.use(require("chai-http"));

const visitData = {
    "BNMID": "AW0123456",
    "Date": "04-15-2023"
};

const malformedVisitData = {
    "WNEID": "AW0123456",
    "Date": "04-15-2023"
};

const conflictVisitData = {
    "BNMID": "AW0123457",
    "Date": "04-15-2023"
};

describe('testcreateVisit', () => {
    it("Should return 201 and create visit", (done) => {
        chai.request('http://localhost:10350')
        .post('/visits')
        .send(visitData)
        .end((error, response) => {
            if(error){
                console.log(error)
                done(error)
            } else {
                console.log(response.body.BNMID)
                chai.assert.equal(response.status, 201, 'visit was created')
                chai.assert.equal(response.body.BNMID, 'AW0123456', 'visit was created')
                chai.assert.equal(response.body.Date, '04-15-2023', 'visit was created')
                done()
            }
        })
    })

    it("Should return 400: Malformed Request", (done) => {
        chai.request('http://localhost:10350')
        .post('/visits')
        .send(malformedVisitData)
        .end((error, response) => {
            if(error){
                console.log(error)
                done(error)
            } else {
                chai.assert.equal(response.status, 400, 'Malformed BNMID entered')
                done()
            }
        })
    })

    it("Should return 409: Conflict", (done) => {
        chai.request('http://localhost:10350')
        .post('/visits')
        .send(conflictVisitData)
        .end((error, response) => {
            if(error){
                console.log(error)
                done(error)
            } else {
                chai.assert.equal(response.status, 409, "Creating visit when guest doesn't exist")
                done()
            }
        })
    })
    
    it("Should only return expected statuses (201, 400, or 409)", (done) => {
        chai.request('http://localhost:10350')
        .post('/visits')
        .send(visitData)
        .end((error, response) => {
            const expectedStatuses = [201, 400, 409];
            if (!expectedStatuses.includes(response.status)) {
                return done(new Error(`Unexpected status code: ${response.status}`));
            }
            done();
        })
    })
});
