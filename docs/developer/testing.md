**Manual Tests** are located in the manual folder.

# Manual Testing Guide

## Set Up Tests

To be able to successfully send the HTTP requests in `testing/manual/calls.http`, the following commands must be done before hand:

1. Make sure you are in the project's root directory in the terminal.
    * To learn how to set up dev the container/environment navigate to development-environment.md in GuestInfoBackend
2. Run the command `./bin/build.sh`
3. Run the command `./bin/up.sh`

These commands creates and run the necessary GuestInfoSystemBackend server, allowing the endpoints to receive requests.

## Correct Order of Tests

In order to test and observe the correct behavior of some tests inside of `calls.http`, other requests must be sent before them. Here are the following requests where this is the case.

### 1. GET and DELETE `/guests/{AW0123456}`

For both of these requests to run as expected, the `POST` request shown below must be first sent.

```http
POST http://localhost:10350/guests HTTP/1.1
content-type: application/json

{
  "BNMID": "AW0123456",
  "Resident": "commuter",
  "Grad_Year": 2023,
  "Grad": "UG",
  "date": "04-12-2023",
  "Year_Issued": 2022
}
```

### 2. GET `/visits/{04-15-2023+04-26-2023}`

For this request to run as expected, both the following `POST` requests shown below must be first sent.

```http
POST http://localhost:10350/visits HTTP/1.1
content-type: application/json

{
  "BNMID": "AW0123456",
  "Date": "04-14-2023"
}
```

```http
POST http://localhost:10350/visits HTTP/1.1
content-type: application/json

{
  "BNMID": "AW7890123",
  "Date": "04-26-2023"
}
```

## Expected Responses

The following is results you should expect from running the tests.
Post requests have been shortened slightly.

`GET http://localhost:10350/version`
Gives the system version and status
The current response is 0.1.0

`GET http://localhost:10350/guests`
Retrieves all the guests. It should be empty by default when there are no guests and populate with guest information when more guests are added.

`GET http://localhost:10350/guests/AW0123456`
Retrieves a specific guest with ID AW0123456. It should return the guest info or gives a 404 error when the guest does not exist.

`DELETE http://localhost:10350/guests/AW0123456`
Deletes a specific guest with ID AW0123456. It should delete the guest with a successful 200 response or gives a 404 error if the guest does not exist.

`POST http://localhost:10350/guests` HTTP/1.1
content-type: application/json {"BNMID": "AW0123456"...}
Posts a guest with ID AW0123456 and returns its info. Gives a 409 conflict when the guest already exists.

`POST http://localhost:10350/guests` HTTP/1.1
content-type: application/json {"BNMID": "AW7890123", "Grad": "UG"...}
Posts a second guest with ID AW7890123 and returns its info. Gives a 409 conflict when the guest already exists.

`PUT http://localhost:10350/guests/AW7890123` HTTP/1.1
content-type: application/json {"BNMID": "AW7890123", "Grad": "G"...}
Updates a second guest with ID AW7890123 and returns its info or gives a 404 error if the guest does not exist.

`GET http://localhost:10350/visits`
Retrieves ALL the visits. It should be empty by default when there are no visits and populate with visit information when more visits are added.

`GET http://localhost:10350/visits/04-15-2023+04-26-2023`
Retrieves ALL the visits for a date range 04-15-2023 ~ 04-26-2023. It should be empty by default when there are no visits and populate with visit information and display a total visit count when more visits are added.

`POST http://localhost:10350/visits` HTTP/1.1
content-type: application/json {"BNMID": "AW0123456", "Date": "04-14-2023"}
Posts a visit with Date 04-14-2023 and ID AW0123456 and returns its info.
Gives error 409 if the ID does not exist.

`POST http://localhost:10350/visits` HTTP/1.1
content-type: application/json {"BNMID": "AW7890123","Date": "04-26-2023"}
Posts a visit with Date 04-26-2023 and ID AW0123456 and returns its info.
Gives error 409 if the ID does not exist.





# Automated Testing

Automated testing is implemented using
[mocha](https://github.com/mochajs/mocha),
[chai](https://github.com/chaijs/chai)
and
[chai-http](https://www.chaijs.com/plugins/chai-http/),
which are JavaScript testing, assertion and HTTP assertion libraries respectively.
Test code can be found in `src/test`, and automatically test the GuestInfoBackend endpoints through 
HTTP requests.

# Automated Testing Guide

Last updated March 2025
These tests use the `mocha` and `chai` libraries and the tests can be found in `src/test`
Follow these steps to run the `automatic tests`:

## Starting the Tests

1. Ensure that your current directory is set to the `bin` folder
2. Initialize dependencies by using `sh build.sh` to install dependencies and start the building process
3. After building, switch your current working directory and set it to the `testing/automatic` folder
4. Run the tests using the command `sh test.sh` to run tests

## Expected Test Results

The following is results you should expect from running these tests.
Included is the test name, the results, and below is the filename the tests come from in `src/test` since the test names vary in their format.

There are 19 passing and 1 failing test

`Create Guest Endpoint`

1. Successfully removes an example guest
2. Successfully posts a new guest when the given data is valid
3. Successfully gives a 409 error if the guest exists already
4. Successfully gives a 400 invalid error if a new guest is invalid and doesn't follow the bnm format
testcreateGuest.js

`testcreateVisit`

1. Successfully creates a visit
2. Successfully gives a 400 error if the request is malformed and includes incorrect data
3. Successfully gives a 409 error if there is a conflicting visit
4. Successfully returns only the expected responses
testcreateVisit.js

`Test deleting guests using their ID`

1. Successfully tests a guest deletion by ID
2. Successfully gives a 404 error if the guest is not found
testdeleteGuest.js

`Test retrieving the API version`

1. Successfully checks the API version
testgetAPIVersion.js

`test POST /guests`

1. Successfully checks an equal integer and might be a leftover or unimplemented test
testGuest.js

`Test retrieving all guests`

1. Successfully retrieves and lists all guests
testlistGuest.js

`testlistVisits /visits`

1. Successfully retrieves and lists all visits
testlistVisits.js

`Test replacing guest data`

1. Successfully creates a new guest to be replaced
2. Successfully updates the guest with new information
3. Fails the error 404 test when the guest does not exist
testreplaceGuest.js

`Test retrieving guest by ID`

1. Successfully retrieves a guest by ID
2. Successfully gives a 404 error when the ID is not found
testretrieveGuest.js

`Rest retrieving visits by date`

1. Successfully retrieves visits
testRetrieveVisit.js
