## This installs dependencies and generates a Docker image.
cd ../../src
npm install --save-dev chai
npm install --save-dev chai-http
npm install --save-dev mocha
cd ../bin
./build.sh
./up.sh
