# Automated Testing Guide

Last updated March 2025
These tests use the `mocha` and `chai` libraries and the tests can be found in `src/test`
Follow these steps to run the `automatic tests`:

## Starting the Tests

1. Ensure that your current directory is set to the `bin` folder
2. Initialize dependencies by using `sh build.sh` to install dependencies and start the building process
3. After building, switch your current working directory and set it to the `testing/automatic` folder
4. Run the tests using the command `sh test.sh` to run tests

## Expected Test Results

The following is results you should expect from running these tests.
Included is the test name, the results, and below is the filename the tests come from in `src/test` since the test names vary in their format.

There are 19 passing and 1 failing test

`Create Guest Endpoint`

1. Successfully removes an example guest
2. Successfully posts a new guest when the given data is valid
3. Successfully gives a 409 error if the guest exists already
4. Successfully gives a 400 invalid error if a new guest is invalid and doesn't follow the bnm format
testcreateGuest.js

`testcreateVisit`

1. Successfully creates a visit
2. Successfully gives a 400 error if the request is malformed and includes incorrect data
3. Successfully gives a 409 error if there is a conflicting visit
4. Successfully returns only the expected responses
testcreateVisit.js

`Test deleting guests using their ID`

1. Successfully tests a guest deletion by ID
2. Successfully gives a 404 error if the guest is not found
testdeleteGuest.js

`Test retrieving the API version`

1. Successfully checks the API version
testgetAPIVersion.js

`test POST /guests`

1. Successfully checks an equal integer and might be a leftover or unimplemented test
testGuest.js

`Test retrieving all guests`

1. Successfully retrieves and lists all guests
testlistGuest.js

`testlistVisits /visits`

1. Successfully retrieves and lists all visits
testlistVisits.js

`Test replacing guest data`

1. Successfully creates a new guest to be replaced
2. Successfully updates the guest with new information
3. Fails the error 404 test when the guest does not exist
testreplaceGuest.js

`Test retrieving guest by ID`

1. Successfully retrieves a guest by ID
2. Successfully gives a 404 error when the ID is not found
testretrieveGuest.js

`Rest retrieving visits by date`

1. Successfully retrieves visits
testRetrieveVisit.js
