# Manual Testing Guide

Last updated March 2025
Follow these steps to run the `manual tests`:

## Starting the Tests

These commands build the GuestInfoSystemBackend server allowing the endpoints to receive requests.

1. Ensure that you are in the project's root directory.
2. Run the command `./bin/build.sh`
3. Run the command `./bin/up.sh`

## Order of Tests

In order to test the functions of tests inside `calls.http`, some tests must be done in a specific order.

When selecting a `guest` with their ID, such as with GET and DELETE `/guests/{AW0123456}`, you must first create the guest using a POST request or it may not appear.

When selecting `visit` by a date range, such as with GET `/visits/{04-15-2023+04-26-2023}`, you must first create the visit using a POST request or it may not appear. You must also first create the related guest with a POST request.

## Expected Responses

The following is results you should expect from running the tests.
Post requests have been shortened slightly.

`GET http://localhost:10350/version`
Gives the system version and status
The current response is 0.1.0

`GET http://localhost:10350/guests`
Retrieves all the guests. It should be empty by default when there are no guests and populate with guest information when more guests are added.

`GET http://localhost:10350/guests/AW0123456`
Retrieves a specific guest with ID AW0123456. It should return the guest info or gives a 404 error when the guest does not exist.

`DELETE http://localhost:10350/guests/AW0123456`
Deletes a specific guest with ID AW0123456. It should delete the guest with a successful 200 response or gives a 404 error if the guest does not exist.

`POST http://localhost:10350/guests` HTTP/1.1
content-type: application/json {"BNMID": "AW0123456"...}
Posts a guest with ID AW0123456 and returns its info. Gives a 409 conflict when the guest already exists.

`POST http://localhost:10350/guests` HTTP/1.1
content-type: application/json {"BNMID": "AW7890123", "Grad": "UG"...}
Posts a second guest with ID AW7890123 and returns its info. Gives a 409 conflict when the guest already exists.

`GET http://localhost:10350/visits`
Retrieves ALL the visits. It should be empty by default when there are no visits and populate with visit information when more visits are added.

`PUT http://localhost:10350/guests/AW7890123` HTTP/1.1
content-type: application/json {"BNMID": "AW7890123", "Grad": "G"...}
Updates a second guest with ID AW7890123 and returns its info or gives a 404 error if the guest does not exist.

`PUT http://localhost:10350/guests/AW7890123` HTTP/1.1
content-type: application/json {"BNMID": "AW7890123", "Grad": "UG"...}
Returns and updates the second guest with ID AW7890123 to its previous info ("UG") or gives a 404 error if the guest does not exist.

`GET http://localhost:10350/visits/04-15-2023+04-26-2023`
Retrieves ALL the visits for a date range 04-15-2023 ~ 04-26-2023. It should be empty by default when there are no visits and populate with visit information and display a total visit count when more visits are added.

`POST http://localhost:10350/visits` HTTP/1.1
content-type: application/json {"BNMID": "AW0123456", "Date": "04-14-2023"}
Posts a visit with Date 04-14-2023 and ID AW0123456 and returns its info.
Gives error 409 if the ID does not exist.

`POST http://localhost:10350/visits` HTTP/1.1
content-type: application/json {"BNMID": "AW7890123","Date": "04-26-2023"}
Posts a visit with Date 04-26-2023 and ID AW0123456 and returns its info.
Gives error 409 if the ID does not exist. It will be included in the get visit range test.

`POST http://localhost:10350/visits` HTTP/1.1
content-type: application/json {"BNMID": "AW7890123","Date": "04-20-2023"}
Posts a visit with Date 04-20-2023 and ID AW0123456 and returns its info.
Gives error 409 if the ID does not exist. It will be included in the get visit range test.
